require 'httparty'

module Bandcamp
  class Base
    include HTTParty
    format :json
    base_uri 'api.bandcamp.com/api'

    class << self
      # NOTE (farleyknight@gmail.com): Bandcamp has officially stopped giving
      # out new developer keys:
      #
      #    http://bandcamp.com/developer#key_request
      #
      # So we'll just hard-code the key they have in their developer guide
      # which fortunately still works
      API_KEY = "vatnajokull"

      def api_key
        API_KEY
      end

      def url(search)
        get("/url/1/info", :query => { :key => Bandcamp::Base.api_key, :url => search })
      end

      def load(options)
        response = get(self.path, :query => { :key => Bandcamp::Base.api_key }.merge(options))
        new(response) if response
      end
    end
  end

  class Band < Base
    attr_reader :name, :url, :band_id, :subdomain, :offsite_url

    def initialize(band)
      @name        = band['name']
      @url         = band['url']
      @band_id     = band['band_id']
      @subdomain   = band['subdomain']
      @offsite_url = band['offsite_url']
    end

    def discography
      response = self.class.get("/band/3/discography", :query => { :key => Bandcamp::Base.api_key, :band_id => band_id })
      return response['discography'] if response && response['discography']
    end

    class << self
      def find(name)
        response = get("/band/3/search", :query => { :key => Bandcamp::Base.api_key, :name => name })
        if response && response['results']
          response['results'].map { |band| new(band) }
        else
          response
        end
      end

      def load(*band_ids)
        response = get("/band/3/info", :query => { :key => Bandcamp::Base.api_key, :band_id => band_ids.join(",") })
        if band_ids.length > 1
          band_ids.map { |band_id|
            new(response[band_id.to_s])
          }
        else
          new(response) if response
        end
      end
    end
  end

  class Album < Base
    attr_reader :title, :release_date, :downloadable, :url, :about, :credits,
                :small_art_url, :large_art_url, :artist, :album_id, :band_id, :tracks

    def initialize(album)
      @title         = album['title']
      @release_date  = Time.at(album['release_date'])
      @downloadable  = album['downloadable']
      @url           = album['url']
      @about         = album['about']
      @credits       = album['credits']
      @small_art_url = album['small_art_url']
      @large_art_url = album['large_art_url']
      @artist        = album['artist']
      @album_id      = album['album_id']
      @band_id       = album['band_id']
      @tracks        = album['tracks'].map{ |track| Track.new(track) }
    end

    def band
      @band ||= Band.load(band_id)
    end

    class << self
      def path
        "/album/2/info"
      end

      def load(id)
        super(:album_id => id)
      end
    end
  end

  class Track < Base
    attr_reader :lyrics, :downloadable, :duration, :about, :album_id, :credits,
                :streaming_url, :number, :title, :url, :track_id, :band_id,
                :release_date, :small_art_url, :large_art_url, :artist

    def initialize(track)
      @lyrics        = track['lyrics']
      @downloadable  = track['downloadable']
      @duration      = track['duration']
      @about         = track['about']
      @album_id      = track['album_id']
      @credits       = track['credits']
      @streaming_url = track['streaming_url']
      @number        = track['number']
      @title         = track['title']
      @url           = track['url']
      @track_id      = track['track_id']
      @band_id       = track['band_id']
      @release_date  = Time.at(track['release_date']) if track['release_date']
      @small_art_url = track['small_art_url']
      @large_art_url = track['large_art_url']
      @artist        = track['artist'] if track['artist']
    end

    def album
      @album ||= Album.load(album_id)
    end

    def band
      @band ||= Band.load(band_id)
    end

    class << self
      def path
        "/track/3/info"
      end

      def load(id)
        super(:track_id => id)
      end
    end
  end
end
